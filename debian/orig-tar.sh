#!/bin/sh -e

VERSION=$2
TAR=../icu4j_$VERSION.orig.tar.gz
DIR=icu4j-$VERSION
TAG=$(echo "release-$VERSION" | sed -re's,\.,-,g')

svn export http://source.icu-project.org/repos/icu/icu4j/tags/${TAG}/ $DIR
tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
